<?php

/**

 * Header.php is generally used on all the pages of your site and is called somewhere near the top

 * of your template files. It's a very important file that should never be deleted.

 *

 * @package Podcaster

 * @since 1.0

 * @author Theme Station

 * @copyright Copyright (c) 2014, Theme Station

 * @link http://www.themestation.co

 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

 */

?>

<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>
	
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

    

    <!-- Mobile Specific -->

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!--[if lt IE 9]>

        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>

    <![endif]-->

    

    <!-- Title Tag -->

    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>

    

    <!-- WP Head -->

    <?php wp_head(); // Very important WordPress core hook. If you delete this bad things WILL happen. ?>

<?php

  if(is_home()){

     echo '<link rel="stylesheet" src="icon.css" />';

  }

?>



<?php /*?><script src="http://localhost/r1o1/wp-content/themes/podcaster/js/jquery-1.4.4.min.js" type="text/javascript"></script>

    <script src="http://localhost/r1o1/wp-content/themes/podcaster/js/jsCarousel.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $('#jsCarousel').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: true });
        });
    </script><?php */?>
</head><!-- /end head -->
<?php if ( is_archive() || is_author() ) : ?>

    <body <?php body_class('podcaster-theme'); ?>>

<?php elseif ( get_post_type() == "podcast" && is_single() ) : ?>      

    <body  <?php body_class('podcast-archive podcaster-theme'); ?>>

<?php elseif( is_page_template('page/pagesidebarleft.php') ) : ?>

    <body  <?php body_class('sidebar-left podcaster-theme'); ?>>

<?php else : ?>

    <body  <?php body_class('podcaster-theme'); ?>>

<?php endif; ?>

<div class="super-container">

<?php /*?><div class="above">
<div class="container">
		
      <div class="row">
		<div class="col-lg-12 co-md-12">
		  <div class="login_area">
                <ul>
        <?php /*if ( is_user_logged_in() ) { ?>
        <?php $current_user = wp_get_current_user(); ?>	 	
    <li><a href="http://localhost/r1o1/profile/"> <?php echo 'Welcome ' . $current_user->user_login . ''; ?></a></li>
    <li><a href="http://localhost/r1o1/profile/">Profile</a></li>
  <?php if ( current_user_can('contributor') ) { ?>  <li><a href="http://localhost/r1o1/game-listing/">My Reviews</a></li><?php } ?>
    <li> <a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logout">Logout</a></li>
    <?php }  else { ?>
    <li><a href="http://localhost/r1o1/register/" class="reg_icn"> Register(Customer) </a></li>
    <li><a href="http://localhost/r1o1/login/" class="login_icn"> Login(Customer/Author) </a></li>
    
    <?php }
    </ul>
   
</div>					
            </div>
        </div>
    </div>
</div><?php */?>
    <?php

        /*Loads the navigation.php template*/

        get_template_part( 'navigation' ); 

    ?>
 
