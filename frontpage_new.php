<?php




/*

Template Name: Front Page New

*/



get_header(); ?>



<?php



if ( have_posts() ) : while ( have_posts() ) : the_post();



$attachment_id = get_post_thumbnail_id( $post->ID );

$image_attributes = wp_get_attachment_image_src( $attachment_id, 'original' ); // returns an array

$thumb_back = $image_attributes[0];



//Header Settings

$subtitle_blurb = get_post_meta($post->ID, 'cmb_thst_page_subtitle', true);

$bg_style = get_post_meta($post->ID, 'cmb_thst_page_header_bg_style', true);

$bg_parallax = get_post_meta($post->ID, 'cmb_thst_page_header_parallax', true);

$heading_align = get_post_meta($post->ID, 'cmb_thst_page_header_align', true);



//For the custom loop

$options = get_option('podcaster-theme');

$arch_category = isset( $options['pod-recordings-category'] ) ? $options['pod-recordings-category'] : '';

$pod_front_num_posts = isset( $options['pod-front-posts'] ) ? $options['pod-front-posts'] : '';





/* Featured Post */

$pod_featured_heading = isset( $options['pod-featured-heading'] ) ? $options['pod-featured-heading'] : '';

$pod_preview_heading = isset( $options['pod-preview-heading'] ) ? $options['pod-preview-heading'] : '';

$pod_preview_title = isset( $options['pod-preview-title'] ) ? $options['pod-preview-title'] : '';

$pod_featured_excerpt = isset( $options['pod-frontpage-fetured-ex'] ) ? $options['pod-frontpage-fetured-ex'] : '';

$pod_page_image = isset( $options['pod-page-image'] ) ? $options['pod-page-image'] : '';





/* Subscribe Buttons */

$pod_butn_one = isset( $options['pod-subscribe1'] ) ? $options['pod-subscribe1'] : '';

$pod_butn_one_url  = isset( $options['pod-subscribe1-url'] ) ? $options['pod-subscribe1-url'] : '';

$pod_butn_two = isset( $options['pod-subscribe2'] ) ? $options['pod-subscribe2'] : '';

$pod_butn_two_url  = isset( $options['pod-subscribe2-url'] ) ? $options['pod-subscribe2-url'] : '';

$pod_fontpage_header = isset( $options['pod-fontpage-header'] ) ? $options['pod-fontpage-header'] : '';





$pod_display_excerpt = isset( $options['pod-display-excerpts'] ) ? $options['pod-display-excerpts'] : '';

$pod_excerpts_style  = isset( $options['pod-excerpts-style'] ) ? $options['pod-excerpts-style'] : '';

$pod_sticky_header = isset( $options['pod-sticky-header'] ) ? $options['pod-sticky-header'] : '';

$pod_frontpage_header  = isset( $options['pod-upload-frontpage-header'] ) ? $options['pod-upload-frontpage-header'] : '';

$pod_frontpage_bg_style = isset( $options['pod-frontpage-bg-style'] ) ? $options['pod-frontpage-bg-style'] : '';

$pod_frontpage_header_par = isset( $options['pod-frontpage-header-par'] ) ? $options['pod-frontpage-header-par'] : '';

if($pod_frontpage_header_par == TRUE){

    $parallax = 'data-stellar-background-ratio="0.5"';

} else {

    $parallax = '';

}



$pod_archive_link = isset( $options['pod-archive-link'] ) ? $options['pod-archive-link'] : '';

$pod_excerpt_type = isset( $options['pod-excerpts-type'] ) ? $options['pod-excerpts-type'] : '';

$pod_nextweek = isset( $options['pod-frontpage-nextweek'] ) ? $options['pod-frontpage-nextweek'] : '';



/* Avatar Settings*/

$pod_avtr_frnt = isset( $options['pod-avatar-front'] ) ? $options['pod-avatar-front'] : '';

$show_avtrs = get_option('show_avatars');

?>



<?php if( class_exists('SeriouslySimplePodcasting') ) : ?>



<?php if ( $pod_sticky_header  == TRUE  ) : ?>

<?php if ( $pod_frontpage_header['url'] != '' && $pod_page_image == false ) : ?>

<div <?php echo $parallax; ?> class="latest-episode sticky front-header" style="background-image: url('<?php echo $pod_frontpage_header['url']; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

    <?php elseif( $pod_page_image == true && $thumb_back !='') : ?>

    <div <?php echo $parallax; ?> class="latest-episode sticky front-header" style="background-image: url('<?php echo $thumb_back; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

        <?php else : ?>

        <div class="latest-episode sticky">

            <?php endif ; ?>

            <?php else : ?>

            <?php if ( $pod_frontpage_header['url'] != '' && $pod_page_image == false ) : ?>

            <div <?php echo $parallax; ?> class="latest-episode front-header" style="background-image: url('<?php echo $pod_frontpage_header['url']; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                <?php elseif( $pod_page_image == true && $thumb_back !='') : ?>

                <div <?php echo $parallax; ?> class="latest-episode front-header" style="background-image: url('<?php echo $thumb_back; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                    <?php else : ?>

                    <div class="latest-episode">

                        <?php endif ; ?>

                        <?php endif ; ?>

                        <div id="loading_bg"></div>

                        <?php if ( $pod_frontpage_header['url'] != '' ) : ?>

                        <div class="translucent">

                            <?php endif ; ?>

                            <div class="container">

                                <div class="row">

                                    <div class="col-lg-12">

                                        <?php



                                        $args = array( 'post_type' => 'podcast', 'posts_per_page' => 1, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                        $category_posts = new WP_Query($args);



                                        if( $category_posts->have_posts() ) : while( $category_posts->have_posts() ) : $category_posts->the_post();



                                            global $ss_podcasting, $wp_query;

                                            $id = get_the_ID();

                                            $file = get_post_meta( $id , 'enclosure' , true );

                                            $terms = wp_get_post_terms( $id , 'series' );

                                            $audiourl = apply_filters( 'thst_the_action', $file );

                                            foreach( $terms as $term ) {

                                                $series_id = $term->term_id;

                                                $series = $term->name;

                                                break;

                                            }

                                            $ep_explicit = get_post_meta( get_the_ID() , 'explicit' , true );

                                            if( $ep_explicit && $ep_explicit == 'on' ) {

                                                $explicit_flag = 'Yes';

                                            } else {

                                                $explicit_flag = 'No';

                                            }





                                            ?>

                                            <div class="main-featured-post clearfix">
				   			<span class="mini-title">

				   				<?php if( isset( $options['pod-featured-heading'] ) ) {

                                    if ( $pod_featured_heading != '' ) {

                                        echo $pod_featured_heading;

                                    } else {

                                        echo __( 'Featured Episode', 'thstlang' );

                                    }

                                } else {

                                    echo __( 'Featured Episode', 'thstlang' );

                                }?>

				   			</span>

                                                <?php if( $explicit_flag == 'Yes' ) { ?>

                                                    <span class="mini-ex">

                                    <?php echo __('Explicit', 'thstlang'); ?>

                                </span>

                                                <?php } ?>

                                                <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

                                                <div class="audio">

                                                    <?php if($file != '') {

                                                        echo '<div class="audio_player">' . do_shortcode('[audio src="' . $file . '"][/audio]</div><!--audio_player-->');

                                                    } ?>

                                                </div><!-- .audio -->

                                                <?php if ( $pod_featured_excerpt == true ) : ?>

                                                    <div class="featured-excerpt">

                                                        <?php if ( $pod_excerpt_type == 'force_excerpt' ) : ?>

                                                            <?php the_excerpt(); ?><a href="<?php echo get_permalink(); ?>" class="more-link"><?php echo __( 'Read More', 'thstlang'); ?><span class="meta-nav"></span></a>

                                                        <?php else : ?>

                                                            <?php global $more;	$more = 0; the_content( __(' Read More', 'thstlang') ); ?>

                                                        <?php endif; ?>

                                                    </div>

                                                <?php endif; ?>

                                            </div><!-- .main-featured-post -->

                                        <?php endwhile; ?>

                                        <?php endif; wp_reset_query(); ?>



                                        <?php if ( $pod_nextweek == 'show' ) : ?>

                                            <div class="next-week">

                                                <div class="row">

                                                    <div class="col-lg-6 col-md-6">

		   						<span class="mini-title">

		   							<?php if( isset( $pod_preview_title ) && $pod_preview_title != '') {

                                        echo $pod_preview_title;

                                    } else {

                                        echo __('Next Time on Podcaster', 'thstlang');

                                    } ?>

		   						</span>



                                                        <h3>

                                                            <?php if( isset ( $pod_preview_heading ) && $pod_preview_heading != '') {

                                                                echo $pod_preview_heading;

                                                            } else {

                                                                echo __('Episode 12: A Long Walk in the Forest', 'thstlang');

                                                            } ?>

                                                        </h3>

                                                    </div><!-- .col -->

                                                    <div class="col-lg-6 col-md-6">

                                                        <div class="content">

                                                            <?php if( isset( $options['pod-subscribe1'] ) ) { ?>

                                                                <a href="<?php echo $pod_butn_one_url ?>" class="butn small"><?php echo $pod_butn_one ?></a>

                                                            <?php } else { ?>

                                                                <a href="#" class="butn small"><?php echo __('Subscribe with iTunes', 'thstlang'); ?></a>

                                                            <?php } ?>

                                                            <?php if( isset( $options['pod-subscribe2'] ) ) { ?>

                                                                <a href="<?php echo $pod_butn_two_url ?>" class="butn small"><?php echo $pod_butn_two ?></a>

                                                            <?php } else { ?>

                                                                <a href="#" class="butn small"><?php echo __('Subscribe with RSS', 'thstlang'); ?></a>

                                                            <?php } ?>

                                                        </div><!-- .content -->

                                                    </div><!-- .col -->

                                                </div><!-- .row -->

                                            </div><!-- .next-week -->

                                        <?php endif; ?>

                                    </div><!-- .col -->

                                </div><!-- .row -->

                            </div><!-- .container -->

                            <?php if ( isset( $pod_frontpage_header ) && is_array( $pod_frontpage_header ) && isset( $pod_frontpage_header['url'] ) && $pod_frontpage_header['url'] != '' ) : ?>

                        </div><!-- .translucent -->

                    <?php endif ; ?>

                    </div><!-- .latest-episode -->

                    <div class="list-of-episodes">

                        <div class="container">

                            <div class="row">

                                <?php

                                $args = array( 'post_type' => 'podcast', 'offset' => 1, 'posts_per_page' => 4, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                $category_posts2 = new WP_Query($args);



                                if( $category_posts2->have_posts() ) : while( $category_posts2->have_posts() ) : $category_posts2->the_post();

                                    global $ss_podcasting, $wp_query;

                                    $id = get_the_ID();

                                    $terms = wp_get_post_terms( $id , 'series' );

                                    foreach( $terms as $term ) {

                                        $series_id = $term->term_id;

                                        $series = $term->name;

                                        break;

                                    }



                                    $ep_explicit = get_post_meta( get_the_ID() , 'explicit' , true );

                                    if( $ep_explicit && $ep_explicit == 'on' ) {

                                        $explicit_flag = 'Yes';

                                    } else {

                                        $explicit_flag = 'No';

                                    }

                                    ?>

                                    <article <?php post_class('col-lg-12 list'); ?>>

                                        <div class="featured-image">

                                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('square'); ?></a>

                                            <div class="hover">

                                                <a href="<?php the_permalink(); ?>" class="batch icon" data-icon="&#xF16b;"></a>

                                            </div><!-- .hover -->

                                        </div><!-- .featured-image -->

                                        <div class="inside">

                                            <div class="post-header">

                                                <?php if( $terms || $explicit_flag == 'Yes' ) : ?>

                                                    <ul>

                                                        <li><?php echo $series; ?></li>

                                                        <li><?php if( $explicit_flag == 'Yes' ) { ?>

                                                                <span class="mini-ex">

			                                <?php echo __('Explicit', 'thstlang'); ?>

			                            </span>

                                                            <?php } ?></li>

                                                    </ul>

                                                <?php endif ; ?>

                                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>



                                            </div><!-- .post-header -->

                                            <div class="post-content">

                                                <?php if ( $pod_excerpt_type == 'force_excerpt' ) : ?>

                                                    <?php the_excerpt(); ?><a href="<?php echo get_permalink(); ?>" class="more-link"><?php echo __( 'Read More', 'thstlang'); ?><span class="meta-nav"></span></a>

                                                <?php else : ?>

                                                    <?php global $more;	$more = 0; the_content( __(' Read More', 'thstlang') ); ?>

                                                <?php endif; ?>

                                            </div>

                                            <div class="post-footer clearfix">

                                                <span class="date"><?php echo get_the_date(); ?></span>

                                            </div><!-- .post-footer -->

                                        </div><!-- .inside -->

                                    </article>

                                <?php endwhile; ?>

                                    <?php $cats_name = get_the_category_by_ID( isset( $arch_category) ) ; ?>

                                    <?php if( isset( $pod_archive_link ) && $pod_archive_link != '' ) : ?>

                                        <div class="button-container col-lg-12">

                                            <a class="butn small" href="<?php echo $pod_archive_link; ?>"><?php echo __('Podcast Archive', 'thstlang') ?></a>

                                        </div>

                                    <?php endif ; ?>

                                <?php endif; wp_reset_query(); ?>



                            </div><!-- .row -->

                        </div><!-- .container -->

                    </div><!-- .list-of-episodes -->

                    <?php elseif( function_exists('powerpress_content') ) : ?>

                    <?php if ( $pod_sticky_header  == TRUE  ) : ?>

                    <?php if ( $pod_frontpage_header['url'] != '' && $pod_page_image == false ) : ?>

                    <div <?php echo $parallax; ?> class="latest-episode sticky front-header" style="background-image: url('<?php echo $pod_frontpage_header['url']; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                        <?php elseif( $pod_page_image == true && $thumb_back !='') : ?>

                        <div <?php echo $parallax; ?> class="latest-episode sticky front-header" style="background-image: url('<?php echo $thumb_back; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                            <?php else : ?>

                            <div class="latest-episode sticky">

                                <?php endif ; ?>

                                <?php else : ?>

                                <?php if ( $pod_frontpage_header['url'] != '' && $pod_page_image == false ) : ?>

                                <div <?php echo $parallax; ?> class="latest-episode front-header" style="background-image: url('<?php echo $pod_frontpage_header['url']; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                                    <?php elseif( $pod_page_image == true && $thumb_back !='') : ?>

                                    <div <?php echo $parallax; ?> class="latest-episode front-header" style="background-image: url('<?php echo $thumb_back; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                                        <?php else : ?>

                                        <div class="latest-episode">

                                            <?php endif ; ?>

                                            <?php endif ; ?>

                                            <div id="loading_bg"></div>

                                            <?php if ( isset( $pod_frontpage_header ) && is_array( $pod_frontpage_header ) && isset( $pod_frontpage_header['url'] ) && $pod_frontpage_header['url'] != '' ) : ?>

                                            <div class="translucent">

                                                <?php endif ; ?>

                                                <div class="container">

                                                    <div class="row">

                                                        <div class="col-lg-12">

                                                            <?php

                                                            if( isset( $arch_category ) ) {

                                                                $args = array( 'cat' => $arch_category, 'posts_per_page' => 1, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                                            } else {

                                                                $args = array( 'posts_per_page' => 1, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                                            }

                                                            $category_posts = new WP_Query($args);

                                                            if($category_posts->have_posts()) : while($category_posts->have_posts()) : $category_posts->the_post();







                                                                $post_format = get_post_format();

                                                                ?>

                                                                <div class="main-featured-post clearfix">

				   			<span class="mini-title">

				   				<?php if( isset( $options['pod-featured-heading'] ) ) {

                                    if ( $pod_featured_heading != '' ) { echo $pod_featured_heading; } else { echo __( 'Featured Episode', 'thstlang' ); }

                                } else {

                                    echo __( 'Featured Episode', 'thstlang' );

                                }?>

				   			</span>

                                                                    <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

                                                                    <a data-icon="&#xF04b;" class="fa-icon listen_butn" href="<?php the_permalink(); ?>"></a><span class="listen_butn_text"><a href="<?php the_permalink(); ?>"><?php echo __('Listen to this Episode', 'thstlang'); ?></a></span>



                                                                    <?php if ( $pod_featured_excerpt == true ) : ?>

                                                                        <div class="featured-excerpt">

                                                                            <?php if ( $pod_excerpt_type == 'force_excerpt' ) : ?>

                                                                                <?php the_excerpt(); ?><a href="<?php echo get_permalink(); ?>" class="more-link"><?php echo __( 'Read More', 'thstlang'); ?><span class="meta-nav"></span></a>

                                                                            <?php else : ?>

                                                                                <?php global $more;	$more = 0; the_content( __(' Read More', 'thstlang') ); ?>

                                                                            <?php endif; ?>

                                                                        </div>

                                                                    <?php endif; ?>

                                                                </div><!-- .main-featured-post -->

                                                            <?php endwhile; ?>

                                                            <?php endif; wp_reset_query(); ?>

                                                            <?php if ( $pod_nextweek == 'show' ) : ?>

                                                                <div class="next-week">

                                                                    <div class="row">

                                                                        <div class="col-lg-6 col-md-6">

		   						<span class="mini-title">

		   							<?php if( isset( $pod_preview_title ) && $pod_preview_title != '') {

                                        echo $pod_preview_title;

                                    } else {

                                        echo __('Next Time on Podcaster', 'thstlang');

                                    } ?>

		   						</span>

                                                                            <h3>

                                                                                <?php if( isset ( $pod_preview_heading ) && $pod_preview_heading != '') {

                                                                                    echo $pod_preview_heading;

                                                                                } else {

                                                                                    echo __('Episode 12: A Long Walk in the Forest', 'thstlang');

                                                                                } ?>

                                                                            </h3>

                                                                        </div><!-- .col -->

                                                                        <div class="col-lg-6 col-md-6">

                                                                            <div class="content">

                                                                                <?php if( isset( $options['pod-subscribe1'] ) ) { ?>

                                                                                    <a href="<?php echo $pod_butn_one_url ?>" class="butn small"><?php echo $pod_butn_one ?></a>

                                                                                <?php } else { ?>

                                                                                    <a href="#" class="butn small"><?php echo __('Subscribe with iTunes', 'thstlang'); ?></a>

                                                                                <?php } ?>

                                                                                <?php if( isset( $options['pod-subscribe2'] ) ) { ?>

                                                                                    <a href="<?php echo $pod_butn_two_url ?>" class="butn small"><?php echo $pod_butn_two ?></a>

                                                                                <?php } else { ?>

                                                                                    <a href="#" class="butn small"><?php echo __('Subscribe with RSS', 'thstlang'); ?></a>

                                                                                <?php } ?>

                                                                            </div><!-- .content -->

                                                                        </div><!-- .col -->

                                                                    </div><!-- .row -->

                                                                </div><!-- .next-week -->

                                                            <?php endif; ?>

                                                        </div><!-- .col -->

                                                    </div><!-- .row -->

                                                </div><!-- .container -->

                                                <?php if ( isset( $pod_frontpage_header ) && is_array( $pod_frontpage_header ) && isset( $pod_frontpage_header['url'] ) && $pod_frontpage_header['url'] != '' ) : ?>

                                            </div><!-- .translucent -->

                                        <?php endif ; ?>

                                        </div><!-- .latest-episode -->



                                        <div class="list-of-episodes">

                                            <div class="container">

                                                <div class="row">

                                                    <?php

                                                    if ( isset( $pod_front_num_posts ) && isset($arch_category) ) {

                                                        $args = array( 'offset' => 1, 'cat' => $arch_category, 'posts_per_page' =>  $pod_front_num_posts, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                                    } else {

                                                        $args = array( 'offset' => 1, 'cat' => 'uncategorized', 'posts_per_page' => 4, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true);

                                                    }



                                                    $category_posts = new WP_Query($args);



                                                    if( $category_posts->have_posts() ) : while( $category_posts->have_posts() ) : $category_posts->the_post();



                                                        $audioex = get_post_meta( $post->ID, 'cmb_thst_audio_explicit', true );

                                                        $videoex = get_post_meta( $post->ID, 'cmb_thst_video_explicit', true );



                                                        ?>

                                                        <article <?php post_class('col-lg-12 list'); ?>>

                                                            <div class="featured-image">

                                                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('square'); ?></a>

                                                                <div class="hover">

                                                                    <a href="<?php the_permalink(); ?>" class="batch icon" data-icon="&#xF16b;"></a>

                                                                </div><!-- .hover -->

                                                            </div><!-- .featured-image -->

                                                            <div class="inside">

                                                                <div class="post-header">

                                                                    <?php if( has_category() ) : ?>

                                                                        <ul>

                                                                            <li><?php the_category(', </li> <li> '); ?></li>

                                                                            <?php if( $audioex == 'on' || $videoex == 'on' ) { ?>

                                                                                <li><span class="mini-ex">

	                                    <?php echo __('Explicit', 'thstlang'); ?>

	                                </span></li>

                                                                            <?php } ?>

                                                                        </ul>

                                                                    <?php endif ; ?>

                                                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                                                                    <?php if( $audioex == 'on' || $videoex == 'on' ) { ?>

                                                                        <span class="mini-ex">

	                                    <?php echo __('Explicit', 'thstlang'); ?>

	                                </span>

                                                                    <?php } ?>



                                                                </div><!-- .post-header -->

                                                                <div class="post-content">

                                                                    <?php if ( $pod_excerpt_type == 'force_excerpt' ) : ?>

                                                                        <?php the_excerpt(); ?><a href="<?php echo get_permalink(); ?>" class="more-link"><?php echo __( 'Read More', 'thstlang'); ?><span class="meta-nav"></span></a>

                                                                    <?php else : ?>

                                                                        <?php global $more;	$more = 0; the_content( __(' Read More', 'thstlang') ); ?>

                                                                    <?php endif; ?>

                                                                </div>

                                                                <div class="post-footer clearfix">

                                                                    <span class="date"><?php echo get_the_date(); ?></span>

                                                                </div><!-- .post-footer -->

                                                            </div><!-- .inside -->

                                                        </article>

                                                    <?php endwhile; ?>

                                                        <?php $cats_name = get_the_category_by_ID( isset( $arch_category) ) ; ?>

                                                        <?php if( isset( $pod_archive_link ) && $pod_archive_link != '' ) : ?>

                                                            <div class="button-container col-lg-12">

                                                                <a class="butn small" href="<?php echo $pod_archive_link; ?>"><?php echo __('Podcast Archive', 'thstlang') ?></a>

                                                            </div>

                                                        <?php endif ; ?>

                                                    <?php endif; wp_reset_query(); ?>



                                                </div><!-- .row -->

                                            </div><!-- .container -->

                                        </div><!-- .list-of-episodes -->

                                        <?php else : /* If Seriously Simple Podcasting is not active use loops below. */ ?>



                                        <?php if ( $pod_sticky_header  == TRUE  ) : ?>

                                        <?php if ( $pod_frontpage_header['url'] != '' && $pod_page_image == false ) : ?>

                                        <div <?php echo $parallax; ?> class="latest-episode sticky front-header" style="background-image: url('<?php echo $pod_frontpage_header['url']; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                                            <?php elseif( $pod_page_image == true && $thumb_back !='') : ?>

                                            <div <?php echo $parallax; ?> class="latest-episode sticky front-header" style="background-image: url('<?php echo $thumb_back; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                                                <?php else : ?>

                                                <div class="latest-episode sticky">

                                                    <?php endif ; ?>

                                                    <?php else : ?>

                                                    <?php if ( $pod_frontpage_header['url'] != '' && $pod_page_image == false ) : ?>

                                                    <div <?php echo $parallax; ?> class="latest-episode front-header" style="background-image: url('<?php echo $pod_frontpage_header['url']; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                                                        <?php elseif( $pod_page_image == true && $thumb_back !='') : ?>

                                                        <div <?php echo $parallax; ?> class="latest-episode front-header" style="background-image: url('<?php echo $thumb_back; ?>'); <?php if(isset($pod_frontpage_bg_style)) echo $pod_frontpage_bg_style; ?> background-position:center;">

                                                            <?php else : ?>

                                                            <div class="latest-episode">

                                                                <?php endif ; ?>

                                                                <?php endif ; ?>

                                                                <div id="loading_bg"></div>

                                                                <?php if ( isset( $pod_frontpage_header ) && is_array( $pod_frontpage_header ) && isset( $pod_frontpage_header['url'] ) && $pod_frontpage_header['url'] != '' ) : ?>

                                                                <div class="translucent">

                                                                    <?php endif ; ?>

                                                                    <div class="container">

                                                                        <div class="row">

                                                                            <div class="col-lg-12">

                                                                                <?php

                                                                                if( isset( $arch_category ) ) {

                                                                                    $args = array( 'cat' => $arch_category, 'posts_per_page' => 1, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                                                                } else {

                                                                                    $args = array( 'posts_per_page' => 1, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                                                                }

                                                                                $category_posts = new WP_Query($args);

                                                                                if($category_posts->have_posts()) : while($category_posts->have_posts()) : $category_posts->the_post();



                                                                                    //For the audio player

                                                                                    $audiourl = get_post_meta( $post->ID, 'cmb_thst_audio_url', true );

                                                                                    $audioembed = get_post_meta( $post->ID, 'cmb_thst_audio_embed', true );

                                                                                    $audioembedcode = get_post_meta( $post->ID, 'cmb_thst_audio_embed_code', true );

                                                                                    $audiocapt = get_post_meta( $post->ID, 'cmb_thst_audio_capt', true );

                                                                                    $audioplists = get_post_meta( $post->ID, 'cmb_thst_audio_playlist', true );

                                                                                    $au_uploadcode = wp_audio_shortcode( $audiourl );



                                                                                    $videoembed = get_post_meta( $post->ID, 'cmb_thst_video_embed', true );

                                                                                    $videoembedcode = get_post_meta( $post->ID, 'cmb_thst_video_embed_code', true );

                                                                                    $videourl = get_post_meta( $post->ID, 'cmb_thst_video_url', true );

                                                                                    $videocapt = get_post_meta($post->ID, 'cmb_thst_video_capt', true);

                                                                                    $videoplists = get_post_meta( $post->ID, 'cmb_thst_video_playlist', true );

                                                                                    $videothumb = get_post_meta($post->ID, 'cmb_thst_video_thumb',true);



                                                                                    $audioex = get_post_meta( $post->ID, 'cmb_thst_audio_explicit', true );

                                                                                    $videoex = get_post_meta( $post->ID, 'cmb_thst_video_explicit', true );



                                                                                    $post_format = get_post_format();

                                                                                    ?>

                                                                                    <div class="main-featured-post clearfix">

                                                                                    </div><!-- .main-featured-post -->

                                                                                <?php endwhile; ?>

                                                                                <?php endif; wp_reset_query(); ?>

                                                                                <?php if ( $pod_nextweek == 'show' ) : ?>

                                                                                    <div class="next-week">

                                                                                        <div class="row">

                                                                                            <?php echo get_podcast_on_header();?>
                                                                                        </div><!-- .row -->

                                                                                    </div><!-- .next-week -->

                                                                                <?php endif; ?>



                                                                            </div><!-- .col -->

                                                                        </div><!-- .row -->

                                                                    </div><!-- .container -->

                                                                    <?php if ( isset( $pod_frontpage_header ) && is_array( $pod_frontpage_header ) && isset( $pod_frontpage_header['url'] ) && $pod_frontpage_header['url'] != '' ) : ?>

                                                                </div><!-- .translucent -->

                                                            <?php endif ; ?>

                                                            </div><!-- .latest-episode -->



                                                            <div class="list-of-episodes">

                                                                <div class="container">

                                                                    <div class="row">

                                                                        <div class="col-lg-12">

                                                                            <div class="row">

                                                                                <div class="col-lg-4 col-md-4">

                                                                                    <h2>Videos</h2>
                                                                                    <?php echo get_latest_r1o1_videos();?>


                                                                                </div>

                                                                                <div class="col-lg-8 col-md-8">

                                                                                    <h2>Review</h2>
                                                                                    <?php echo get_latest_r1o1_games();?>




                                                                            </div>




                                                                    </div><!-- .row -->



                                                                </div><!-- .container -->

                                                            </div><!-- .list-of-episodes -->

                                                            <?php endif; /* End of 'Is Seriously Simple Podcasting active?' */ ?>













                                                            <?php if (  isset( $pod_excerpts_style ) && $pod_excerpts_style == 'columns' ) : ?>

                                                            <div class="fromtheblog">

                                                                <div class="container">

                                                                    <div class="row">

                                                                        <div class="col-lg-12">

                                                                            <h2 class="title"><?php echo __('Videos', 'thstlang') ?></h2>

                                                                            <div class="row">



                                                                                <?php

                                                                                if( isset( $arch_category ) ) {

                                                                                    $args = array( 'cat' => -$arch_category, 'posts_per_page' => 4, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true);

                                                                                } else {

                                                                                    $args = array( 'posts_per_page' => 4, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                                                                }

                                                                                //echo $arch_category;

                                                                                $fromblog_posts = new WP_Query($args);



                                                                                if( $fromblog_posts->have_posts() ) : while( $fromblog_posts->have_posts() ) : $fromblog_posts->the_post(); ?>

                                                                                    <article <?php post_class('col-lg-3'); ?>>

                                                                                        <div class="featured-image">

                                                                                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('square'); ?></a>

                                                                                        </div><!-- .featured-image -->

                                                                                        <div class="inside">

                                                                                            <div class="post-header">

                                                                                                <?php if( has_category() ) : ?>

                                                                                                    <ul>

                                                                                                        <li><?php the_category(', </li> <li> '); ?></li>

                                                                                                    </ul>

                                                                                                <?php endif ; ?>

                                                                                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>



                                                                                            </div><!-- .post-header -->

                                                                                            <div class="post-content">

                                                                                                <?php if ( $pod_excerpt_type == 'force_excerpt' ) : ?>

                                                                                                    <?php the_excerpt(); ?>

                                                                                                <?php else : ?>

                                                                                                    <?php global $more;	$more = 0; the_content(''); ?>

                                                                                                <?php endif; ?>

                                                                                            </div>

                                                                                            <div class="post-footer clearfix">

                                                                                                <a href="<?php the_permalink(); ?>"><?php echo __('Read More', 'thstlang') ?></a>

                                                                                            </div><!-- .post-footer -->

                                                                                        </div><!-- .inside -->

                                                                                    </article>

                                                                                <?php endwhile; ?>

                                                                                <?php endif; wp_reset_query(); ?>

                                                                            </div><!-- .col-->

                                                                        </div><!-- .col -->

                                                                    </div><!-- .row -->

                                                                </div><!-- .container -->

                                                            </div><!--coral-->

                                                        </div>

                                                    <?php elseif( isset( $pod_excerpts_style ) && $pod_excerpts_style == 'list' ) : ?>

                                                        <div class="fromtheblog list">

                                                            <div class="container">

                                                                <div class="row">

                                                                    <div class="col-lg-12">

                                                                        <h2 class="title"><?php echo __('Videos', 'thstlang') ?></h2>



                                                                        <?php

                                                                        if ( isset( $arch_category ) ) {

                                                                            $args = array( 'cat' => 11 , 'posts_per_page' => 4, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                                                        } else {

                                                                            $args = array( 'cat' => 11 , 'posts_per_page' => 4, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true );

                                                                        }

                                                                        $fromblog_posts = new WP_Query($args);



                                                                        if( $fromblog_posts->have_posts() ) : while( $fromblog_posts->have_posts() ) : $fromblog_posts->the_post(); ?>

                                                                            <article <?php post_class(); ?>>

                                                                                <div class="inside clearfix">

                                                                                    <div class="cont post-header">

                                                                                        <?php if( $show_avtrs == true && $pod_avtr_frnt == true ) : ?>

                                                                                            <a class="user_img_link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">

                                                                                                <?php

                                                                                                $usr_avatar = get_avatar( get_the_author_meta( 'ID' ), 32 );

                                                                                                echo $usr_avatar;

                                                                                                ?>

                                                                                            </a>

                                                                                        <?php endif; ?>

                                                                                        <span><?php the_author(); ?></span>

                                                                                    </div>

                                                                                    <div class="cont_large post-content">

                                                                                        <span class="cats"><?php the_category('</span> <span class="cats"> '); ?></span>

                                                                                        <span class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>

                                                                                    </div><!-- .post-header -->



                                                                                    <div class="cont date post-footer">

                                                                                        <span><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>

                                                                                    </div><!-- .post-footer -->

                                                                                </div><!-- .inside -->

                                                                            </article>

                                                                        <?php endwhile; ?>

                                                                        <?php endif; wp_reset_query(); ?>



                                                                        <?php if( get_option( 'show_on_front' ) == 'page' ) { ?>

                                                                            <div class="button-container">

                                                                                <a class="butn small" href="<?php echo get_permalink( get_option('page_for_posts' ) ); ?>"><?php echo __('Go to Blog', 'thstlang') ?></a>

                                                                            </div>

                                                                        <?php } ?>

                                                                    </div><!-- .col -->

                                                                </div><!-- .row -->

                                                            </div><!-- .container -->

                                                        </div>

                                                    <?php else : ?>

                                                        <?php //Do nothing. ?>

                                                    <?php endif; ?>



                                                        <?php endwhile; else: ?>

                                                            <p>Sorry, no posts matched your criteria.</p>

                                                        <?php endif; ?>



                                                        <?php get_footer(); ?>



                                                        <style>.authot_image_none { display:none !important;}



                                                        </style>