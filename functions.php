<?php
add_image_size('game_main_thumb',750,200,true);
add_image_size('game_small_thumb',180,160,true);
add_image_size('video_large_thumb',446,200,true);
add_image_size('video_small_thumb',214,128,true);

function get_header_social_links_icons(){
    $options = get_option('podcaster-theme');
    $output='<div class="social_header_icons">';
    array('facebook','youtube','soundcloud','instagram','twitter','google','tumblr','pinterest','flickr','vimeo','skype','dribbble Icon','weibo','foursquare','github','xing');
    $newArr=array('soundcloud','youtube','instagram','twitter');
    foreach ($newArr as $val) {
        $data=$options['pod-'.$val];
        if(!empty($data)){
            $output.='<a href="'.$data.'" class="r1o1_social_icons icon-'.$val.'" target="_blank">'.$val.'</a>';
        }
    }

    $output.='</div>';
    return $output;
}
function get_podcast_on_header(){
    $divOut='';
    $listOut='';
    $args = array( 'offset' => 1, 'cat' => 1,'post_type'=>'post', 'posts_per_page' => 4, 'paged' => get_query_var( 'paged' ), 'ignore_sticky_posts' => true);
    $count=1;
    $category_posts = new WP_Query($args);
    if( $category_posts->have_posts() ) : while( $category_posts->have_posts() ) : $category_posts->the_post();
        $id = get_the_ID();
        if(isset($_GET['pod'])){
            $podId=$_GET['pod'];
        } elseif($count==1){
            $podId=$id;
        }

        $file = apply_filters( 'thst_the_action', get_post_meta( $id, 'cmb_thst_audio_url', true ) );

        if($id==$podId) {
            $divOut.='<div id="podcast_player_'.$id.'" class="podcast_player">';
            if ($file != '') {
                $divOut .= '<div class="audio_player">' . do_shortcode('[audio src="' . $file . '"][/audio]</div>');
            }
            $divOut .= '<div class="podcast_content">' . get_the_excerpt() . '<a href="' . get_permalink() . '" class="more-link">' . __('Read More', 'thstlang') . '<span class="meta-nav"></span></a></div>';
            $class=" podcart_act";
            $divOut.='</div>';
        } else{
            $class='';
        }

        $url=add_query_arg('pod',$id,site_url());
        $listOut.='<a href="'.$url.'" class="podcast_init'.$class.'">'.short_limit_new(19).'</a>';
        $count++;
    endwhile;
    endif;
    $out='<div class="front_page_podcast">'.$divOut.'<div class="podcast_list">'.$listOut.'<div class="more_games"><a href="'.get_category_link( 1 ).'" class="podcast_init">More</a></div></div></div>';
    return $out;
}


function get_latest_r1o1_games(){

    if(!function_exists('ExecuteGetRows')) {
        function ExecuteGetRows($sql)

        {

            $sqlquery = $sql;

            $executes = mysql_query($sqlquery) or die(mysql_error());

            $i = 0;

            $result = array();

            while ($res = @mysql_fetch_assoc($executes)) {

                $result[$i] = $res;

                $i++;

            }

            return $result;

        }
    }



    if(!function_exists('get_single_val')) {
        function get_single_val($tbl_name, $desire_column, $compare_column, $compare_vale)

        {

            $sql_res = "SELECT " . $desire_column . " FROM " . $tbl_name . " WHERE $compare_column = '" . $compare_vale . "'";

            $res = ExecuteGetRows($sql_res);

            return $res[0][$desire_column];

        }
    }
    $output='<div class="r1o1_games">';
    $port = new WP_Query(array('post_type' => 'game', 'posts_per_page' => 4));
    $count=1;
    $authorData='';
    if ($port->have_posts()):
        while ($port->have_posts()) :
            $port->the_post();
            $postid=get_the_ID();
            if($count==1){
                $author_sql="SELECT * FROM wp_rewiew WHERE current_post = $postid";

                $author_sql_sql	    = ExecuteGetRows($author_sql);
                if(is_array($author_sql_sql)) {
                    foreach ($author_sql_sql as $k => $val) {
                        if($k<6) {
                            $author = $val['current_user'];
                            $authorData .= '<div class="author_image">' . get_wp_user_avatar($author, 'thumbnail') . '</div>';
                        }
                    }
                }
                $output.='<div class="main_game_thumb"><div class="thumb_image">';
                $output.= get_the_post_thumbnail($postid,'game_main_thumb').'</div>';
                $output.='<div class="author_desc">'.$authorData.'</div>';
                $output.='<div class="img_title"><a href="'.get_permalink().'">'.get_the_title().'</a></div>';
                $output.='</div>';
            }
            else{
                $reMeta='';
                $reviewd=get_post_meta( $postid, 'reviewd_on', true );
                if(!empty($reviewd)){
                    $reMeta.='<div class="game_review">';
                    $arr= explode(',',$reviewd);
                    foreach($arr as $val){
                        $reMeta.='<span class="review_val">'.trim($val).'</span>';
                    }
                    $reMeta.='<label>:Reviewed On</label></div>';
                }
                $output.='<div class="small_game_thumb">';
                $output.='<div class="game_content_div"><div class="img_title"><a href="'.get_permalink().'">'.get_the_title().'</a></div>';
                $output.=$reMeta;
                $output.='<div class="game_content">'.custom_excerpt(20,'').'</div>';
                $output.='<div class="game_time">'.get_the_time('F j, Y').'</div>';
                $output.='</div>';
                $output.= '<div class="thumb_image">'.get_the_post_thumbnail($postid,'game_small_thumb').'</div>';

                $output.='</div>';
            }
            $count++;
        endwhile;
    endif;
    $output.='<div class="more_games"><a href="'.get_permalink( get_page_by_path( 'games' ) ).'">More</a></div>';
    $output.='</div>';
    return $output;
}
function get_latest_r1o1_videos(){
    $output='<div class="r1o1_videos">';
    $count = 1; $port1 = new WP_Query( array( 'post_type' => 'post', 'cat' => 11 , 'posts_per_page' => 9 ));
    if($port1->have_posts()):
        while ( $port1->have_posts() ) : $port1->the_post();

            $postid=get_the_ID();
            if($count==1){

                $output.='<div class="main_video_thumb"><div class="thumb_image">';
                $output.= get_the_post_thumbnail($postid,'video_large_thumb').'</div>';

                $output.='<div class="img_title"><a href="'.get_permalink().'">'.get_the_title().'</a></div>';
                $output.='</div>';
            }
            else{


                $output.='<div class="small_video_thumb">';
                $output.= '<div class="transparent_bg_img"></div><div class="thumb_image">'.get_the_post_thumbnail($postid,'video_small_thumb').'</div>';
                $output.= '<a class="video_play_button"  href="'.get_permalink().'">Play Button</a>';
                $output.='<div class="img_title"><a href="'.get_permalink().'">'.get_the_title().'</a></div>';
                $output.='</div>';
            }
            $count++;
        endwhile;
    endif;
    $output.='<div class="more_games"><a href="'.get_category_link( 11 ).'">More</a></div>';
    $output.='</div>';
    return $output;
}

function short_title($limit) {
    $title = get_the_title();
    $title = html_entity_decode($title, ENT_QUOTES, "UTF-8");

    // $limit = "49";
    $pad="...";

    if(strlen($title) >= ($limit+3)) {
        $title = substr($title, 0, $limit); }

    return $title;
}
function short_limit_new($limit){
    $title = get_the_title();
    $arr = str_split($title);
    $newArr= array_slice($arr, 0, $limit,true);
    $titleNew=implode("", $newArr);
    return $titleNew;
}
function custom_excerpt($new_length = 20, $new_more = '...') {
    add_filter('excerpt_length', function () use ($new_length) {
        return $new_length;
    }, 999);
    add_filter('excerpt_more', function () use ($new_more) {
        return $new_more;
    });
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    return $output;
}


function mp_add_game_review() {
    add_meta_box(
        'add_game_review',
        __( 'Game Reviewed On', '' ),
        'mp_add_game_post_custom',
        'game','normal'
    );

}
add_action( 'add_meta_boxes', 'mp_add_game_review' );

function mp_add_game_post_custom($post_id){
    global $post;
    $value = get_post_meta( $post->ID, 'reviewd_on', true );
    ?>
    <label>Reviewd On</label>
    <input type="text" name="reviewd_on" id="reviewd_on" value="<?php echo $value;?>">



<?php
}
function mp_save_product_brands( $post_id ) {


    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'game' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */

    // Make sure that it is set.
    if ( ! isset( $_POST['reviewd_on'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = $_POST['reviewd_on'];

    // Update the meta field in the database.
    update_post_meta( $post_id, 'reviewd_on', $my_data );
}
add_action( 'save_post', 'mp_save_product_brands' );


add_action('wp_enqueue_scripts','include_stylesheet_script');
function include_stylesheet_script(){

	wp_enqueue_style('bootstrap.min',get_stylesheet_directory_uri() .'/css/bootstrap.min.css');
	wp_enqueue_script('email-follow',get_stylesheet_directory_uri() .'/js/email-follow.js');
}
add_action( 'after_setup_theme', 'mtb_register_my_menu' );
function mtb_register_my_menu() {
    register_nav_menus( array(
        'header_menu1' => 'Header Menu1',
        'footer_menu1' => 'Footer Menu1',
    ) );
}

add_action( 'widgets_init', 'my_register_sidebars_email' );
function my_register_sidebars_email() {
	register_sidebar(		
		array(			
			'id' => 'email_subscriber',			
			'name' => __( 'Email Subscriber', 'thstlang' ),			
			'description' => __( 'This sidebar will appear on your pages.', 'thstlang' ),			
			'before_widget' => '<div id="%1$s" class="widget %2$s">',			
			'after_widget' => '</div>',			
			'before_title' => '<h3>',			
			'after_title' => '</h3>'		
		)	
	);	
}

add_action('wp_head','r1o1_set_ajaxurl');
function r1o1_set_ajaxurl() {
?>
<script type="text/javascript">
var r1o1_ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php
}

add_action('wp_ajax_r1o1_submit_review','callback_r1o1_submit_review');
add_action('wp_ajax_nopriv_r1o1_submit_review','callback_r1o1_submit_review');
function callback_r1o1_submit_review(){

//var_dump($_POST['thing']);
  if(isset($_POST['add_catgory_chk']))
{
	 
			$final_review=$_POST['final_review'];
			$image_rating=$_POST['image_rating'];
			$current_user=$_POST['current_user'];
			$current_post=$_POST['current_post'];
			
			$value=$_POST['thing'];
			$goodthings=serialize($value['good_thing']);
			$bedthings=serialize($value['bad_thing']);
			 if (count($value> 0))
    {
        $new = array();
       
        
            $new[] = "('" . $final_review . "', '" . $image_rating . "', '" . $current_user . "', '" . $current_post . "', '" . $goodthings . "', '" . $bedthings ."')";
        
        if (count($new) > 0)
        {
            $query = mysql_query("INSERT INTO wp_rewiew (`final_review`,`ratting`, `current_user`, `current_post`,`good_thing`, `bad_thing`) VALUES " . implode(", ", $new));
            if ($query)
            {
                echo "SUCCESS";
            }
            else
            {
                echo "FAILED";
            }
        }
    }
}

if(isset($_POST['edit_catgory_chk']))
{
	
		 
		
		$current_user = $_POST['current_user_1'];
	//   echo $current_user;
		
		$sql_insert = "UPDATE `wp_rewiew` SET `good_thing` = N'".addslashes($_POST['good_thing'])."', `bad_thing` = N'".addslashes($_POST['bad_thing'])."',
`final_review` = N'".addslashes($_POST['final_review'])."', `ratting` = ".addslashes($_POST['image_rating'])." WHERE `current_user` = $current_user";
			
				//echo $sql_insert;	
		mysql_query($sql_insert) or die(mysql_error());
		  if(mysql_query($sql_insert))
		 {
		   echo 1;
		 }
		 else
		 {
		   echo 3;
		 }
	
}

die();
}




