<?php
/**
 * This file displays your single posts.
 *
 * @package Podcaster
 * @since 1.0
 * @author Theme Station 
 * @copyright Copyright (c) 2014, Theme Station
 * @link http://www.themestation.co
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
/* Loads the header.php template*/
get_header();
$options = get_option('podcaster-theme');  
$thst_wp_version = get_bloginfo( 'version' );
$format = get_post_format();
$videoembed = get_post_meta( $post->ID, 'cmb_thst_video_embed', true);
$videourl =  get_post_meta( $post->ID, 'cmb_thst_video_url', true);
$videocapt = get_post_meta($post->ID, 'cmb_thst_video_capt', true);
$videothumb = get_post_meta($post->ID, 'cmb_thst_video_thumb',true);
$videoplists = get_post_meta( $post->ID, 'cmb_thst_video_playlist', true );
$videoembedcode = get_post_meta( $post->ID, 'cmb_thst_video_embed_code', true );
$audiourl = apply_filters( 'thst_the_action', get_post_meta( $post->ID, 'cmb_thst_audio_url', true ) );
$audioembed = apply_filters( 'thst_the_action', get_post_meta( $post->ID, 'cmb_thst_audio_embed', true ) );
$audioembedcode = get_post_meta( $post->ID, 'cmb_thst_audio_embed_code', true );
$audiocapt = get_post_meta($post->ID, 'cmb_thst_audio_capt', true);
$audioplists = get_post_meta( $post->ID, 'cmb_thst_audio_playlist', true );
$galleryimgs = get_post_meta( $post->ID, 'cmb_thst_gallery_list', true );
$gallerycapt = get_post_meta($post->ID, 'cmb_thst_gallery_capt', true);
$gallerycol = get_post_meta($post->ID, 'cmb_thst_gallery_col', true);
if ( isset( $options['pod-pofo-gallery'] ) ) {
	$gallerystyle = $options['pod-pofo-gallery'];
}
$thump_cap = get_post(get_post_thumbnail_id())->post_excerpt;
if(isset($options['pod-sticky-header'])){
	$pod_sticky_header = $options['pod-sticky-header'];
}
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
if( isset( $options['pod-single-header-display'] ) ){
	$pod_single_header_display = $options['pod-single-header-display'];
}
if( isset( $options['pod-single-header-par'] ) ){
	$pod_single_header_par = $options['pod-single-header-par'];
}
if( isset( $options['pod-single-bg-style'] ) ){
	$pod_single_bg_style = $options['pod-single-bg-style'];
}
 ?>
	<?php if ( $format == "video" || $format == "image" || $format == "audio" ) : ?>
		<?php if ( $format == "video" || $format == "audio" ) : ?>
			<?php if ( isset ( $pod_sticky_header ) &&  $pod_sticky_header == TRUE ) : ?>
				<div <?php if( isset( $pod_single_header_par ) && $pod_single_header_par == TRUE ) { ?>data-stellar-background-ratio="0.25"<?php } ?> class="single-featured sticky <?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'background' ) echo 'thumb_bg'; ?>" <?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'background') { ?>style="background-image: url(<?php  echo $image[0]; ?>); <?php if( isset( $pod_single_bg_style ) ) echo $pod_single_bg_style; ?>"<?php } ?>>
			<?php else :  ?>
				<div <?php if( isset( $pod_single_header_par ) && $pod_single_header_par == TRUE ) { ?>data-stellar-background-ratio="0.25"<?php } ?> class="single-featured <?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'background' ) echo 'thumb_bg'; ?>" <?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'background') { ?>style="background-image: url(<?php  echo $image[0]; ?>); <?php if( isset( $pod_single_bg_style ) ) echo $pod_single_bg_style; ?>"<?php } ?>>
			<?php endif ; ?>
		<?php elseif( $format == "image" ) : ?>
			<?php if ( isset ( $pod_sticky_header ) &&  $pod_sticky_header == TRUE ) : ?>
				<div class="single-featured sticky <?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'background' ) echo 'thumb_bg'; ?>">
			<?php else :  ?>
				<div class="single-featured <?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'background' ) echo 'thumb_bg'; ?>">
			<?php endif ; ?>
		<?php endif; ?>
		<?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'background' ) : ?>
			<div class="background">
		<?php endif ; ?>
		<?php if ( $format == "video" && ($videoembed != '' || $videourl != '' || $videoplists != '' || $videoembedcode != '' )  ) : ?>
			<div class="container">
				<div class="row">						
					<div class="col-lg-12">
						<?php 
						if($videoembed != '') {
							echo '<div class="video_player">' . wp_oembed_get($videoembed) . '</div><!--video_player-->';
						}
						if($videourl != '') {
							echo '<div class="video_player">' . do_shortcode('[video poster="' .$videothumb. '" src="' .$videourl. '"][/video]') .'</div><!--video_player-->';
						}
						if( is_array( $videoplists ) ) {
							echo do_shortcode('[playlist type="video" ids="'.implode(',', array_keys($videoplists)).'"][/playlist]');
						} elseif( $videoembedcode !='') {
							echo '<div class="video_player">' . $videoembedcode . '</div><!--video_player-->';									
						}
						?>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		<?php endif; ?>
				
		<?php if ( $format == "image" ) : ?>	
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="image">
						<?php if ( has_post_thumbnail( $post->ID ) ) : ?>
						<?php 
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'regular-large' );
							the_post_thumbnail( 'regular-large' ); ?>
						<?php endif; ?>
						</div><!-- .image -->
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		<?php endif; ?>
					
		<?php if ( $format == "audio" ) : ?>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'thumbnail' ) : ?>
							<div class="album-art clearfix">
							<?php the_post_thumbnail('square-large'); ?>
							</div>
						<?php endif ; ?>
						<div class="player_container<?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'thumbnail' ) echo ' with_thumbnail'; ?>">
							<span class="mini-title"><?php echo get_the_date(); ?></span>
							<h2><?php the_title(); ?></h2>	
							<div class="audio">				
								<?php if($audioembed != '') {
									$au_embedcode = wp_oembed_get( $audioembed );
									echo '<div class="audio_player au_oembed">' . $au_embedcode . '</div><!--audio_player-->';
								}
								if($audiourl != '') {
									echo '<div class="audio_player">' . do_shortcode('[audio src="' .$audiourl. '"][/audio]</div><!--audio_player-->');
								} 
								if( is_array( $audioplists ) ) {
									echo do_shortcode('[playlist type="audio" ids="'.implode(',', array_keys($audioplists)).'"][/playlist]');
								} 
								if ( $audioembedcode != '') {
									echo '<div class="audio_player embed_code">' . $audioembedcode . '</div><!--audio_player-->';
								} 
								?>
							</div><!-- .audio -->
						</div><!-- .player_container -->
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		<?php endif; ?>
		<?php if( has_post_thumbnail() && isset( $pod_single_header_display ) && $pod_single_header_display == 'background' ) : ?>
		</div><!-- .background -->
		<?php endif ; ?>
		</div><!-- .single-featured -->
	<?php elseif( $format == "gallery" ) : ?>
		<?php if ( isset( $pod_sticky_header ) && $pod_sticky_header == TRUE ) : ?>
			<div class="single-featured gallery sticky">
		<?php else : ?>
			<div class="single-featured gallery">
		<?php endif ; ?>
		<?php if ( $galleryimgs != ''  ) : ?>
			<div class="featured-gallery">
				<?php if ( $gallerystyle == "grid_on" ) : ?>
					<div class="gallery grid clearfix <?php echo  $gallerycol; ?> ">
						<?php 
							foreach ($galleryimgs as $galleryimgsKey => $galleryimg) {
								$imgid = $galleryimgsKey;
					    		echo '<div class="gallery-item">';
					    			
					    		echo '<a href="' . $galleryimg . '" data-lightbox="lightbox">';
					    		echo wp_get_attachment_image( $imgid, 'square-large' );
					    		echo '</a>';
					    		echo '</div>';
							}
						?>
				<?php else : ?>
					<div class="gallery flexslider">
						<ul class="slides">
							<?php 
								foreach ($galleryimgs as $galleryimgsKey => $galleryimg) {
									$imgid = $galleryimgsKey;
					    			echo '<li>';
					    			echo wp_get_attachment_image( $imgid, 'regular-large' );
					    			echo '</li>';
								}
							?>
						</ul>
					</div><!-- .gallery.flexslider -->
				<?php endif; ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>
			
	
	<?php if($videocapt != '' || $audiocapt != '' || $thump_cap !='' || $gallerycapt !=''  ) : ?>
		<div class="caption-container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div>
					   		<?php if ( $format == "video" ) : ?>
								<?php echo '<div class="featured vid">' . $videocapt . '</div>'; ?>
							<?php endif; ?>
							<?php if ( $format == "audio" ) : ?>
								<?php echo '<div class="featured audio">' . $audiocapt . '</div>'; ?>
							<?php endif; ?>
							<?php if ( $format == "image" ) : ?>
								<?php echo '<div class="featured img">' . $thump_cap . '</div>'; ?>
							<?php endif; ?>
							<?php if ( $format == "gallery" ) : ?>
								<?php echo '<div class="featured img">' . $gallerycapt . '</div>'; ?>
							<?php endif; ?>
					   	</div><!-- .next-week -->
					</div><!-- .col -->
				</div><!-- .row -->	 
			</div><!-- .container -->  	
		</div>
	<?php endif; ?>
	
	<?php if ( ! ($format == "video" || $format == "image" || $format == "audio" || $format == "gallery") &&  isset( $pod_sticky_header) == TRUE ) : ?>
	<div class="thst-main-posts sticky">
	<?php else : ?>
	<div class="thst-main-posts">
	<?php endif ; ?>
		<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8">
						<div class="content">
							<?php
							/*The following line creates the main loop.*/
							if (have_posts()) :
								while (have_posts()) : the_post();
								
								/*This gets the template to display the posts.*/
								get_template_part( 'post/format', $format );
								?>
								<?php setPostViews(get_the_ID()); ?>
								<?php 
								endwhile;	
							endif;
							?>
							<?php comments_template(); ?> 
						</div><!-- .content-->
					</div>
					
					<div class="col-lg-4 col-md-4">
					<?php
						/*This displays the sidebar with help of sidebar.php*/
						get_template_part('sidebar');
					?>
					</div><!--span4-->
				</div><!--row-->
		</div><!--container-->
	</div><!--thst-main-posts-->
<?php
/*This displays the footer with help of footer.php*/
get_footer(); ?>