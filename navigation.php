<?php
/**
 * This file is used to display the navigation.
 *
 * @package Podcaster
 * @since 1.0
 * @author Theme Station
 * @copyright Copyright (c) 2014, Theme Station
 * @link http://www.themestation.co
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

$options = get_option('podcaster-theme');  

if(isset($options['pod-upload-logo'])){
	$pod_upload_logo_url = $options['pod-upload-logo'];
}
if(isset($options['pod-sticky-header'])){
	$pod_sticky_header = $options['pod-sticky-header'];
}

 ?>
<?php if( isset( $pod_sticky_header ) && $pod_sticky_header == TRUE ) : ?>
<div class="above large_nav">
<?php else : ?>
<div class="above">
<?php endif ; ?>
	<div class="container">
		<div class="row">

			<div class="col-lg-5 col-md-3">
				<header class="header" id="top" role="banner">
					<a href="#" id="open-off-can" class="open-menu batch" data-icon="&#xF0AA;"></a>
					<?php if ( isset( $pod_upload_logo_url ) && $pod_upload_logo_url != '' ) : ?>
					<h1 class="main-title logo">
					<?php else : ?>
					<h1 class="main-title">
					<?php endif ; ?>
						<a href="<?php echo home_url(); ?>/" title="<?php get_bloginfo( 'name' ); ?>" rel="home"><?php echo get_bloginfo( 'name' ); ?></a>
					</h1>
				</header><!--header-->
			</div><!--col-lg-3-->

			<div class="col-lg-7 co-md-9">
			<?php /*	<nav id="nav" class="navigation" role="navigation">
					<?php if ( has_nav_menu( 'header-menu' ) ) { ?>					
						<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_id' => 'res-menu', 'sort_column' => 'menu_order', 'menu_class' => 'thst-menu', 'fallback_cb' => false, 'container' => false )); ?>						
					<?php } else { ?>
						<div class="menu">
							<a href="<?php echo $url = admin_url( 'nav-menus.php', 'admin' ); ?>">Please click here to create and set your menu</a>
						</div>
					<?php } ?>
				</nav><!--navigation--> */?>
                <?php echo get_header_social_links_icons();?>
			</div><!--col-lg-9-->

		</div><!--row-->
	</div><!--container-->
    <div class="menu_top">
<div class="menu_style">
<div class="follow_menu_area">
<div class="email_subscriber_class">Follow</div>
 <div id="email_subscriber" style="display:none"> 
<?php dynamic_sidebar( 'email_subscriber' ); ?>
</div>
<?php get_search_form(); ?>
</div>
<?php wp_nav_menu( array('theme_location' => 'header_menu1','items_wrap'=> '%3$s' )); ?>
</div>
</div>
</div>
