<?php
/**
 * This file is used to display audio post format.
 * @package Podcaster
 * @since 1.0
 * @author Theme Station : http://www.themestation.net
 * @copyright Copyright (c) 2014, Theme Station
 * @link http://www.themestation.net
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
 
 $audiourl = get_post_meta( $post->ID, 'cmb_thst_audio_url', true );
 $audioembed = get_post_meta( $post->ID, 'cmb_thst_audio_embed', true );
 $audioembedcode = get_post_meta( $post->ID, 'cmb_thst_audio_embed_code', true );
 $audiocapt = get_post_meta( $post->ID, 'cmb_thst_audio_capt', true );
 $audioplists = get_post_meta( $post->ID, 'cmb_thst_audio_playlist', true );
 $au_uploadcode = wp_audio_shortcode( $audiourl );

 $options = get_option('podcaster-theme');
 $pod_butn_one = isset( $options['pod-subscribe1'] ) ? $options['pod-subscribe1'] : '';
 $pod_butn_one_url = isset( $options['pod-subscribe1-url'] ) ? $options['pod-subscribe1-url'] : '';
 $pod_subscribe_single = isset( $options['pod-subscribe-single'] ) ? $options['pod-subscribe-single'] : '';
 ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php 
			$pod_download = get_post_meta( $post->ID, 'cmb_thst_audio_download', true );
			$pod_filedownload = get_post_meta( $post->ID, 'cmb_thst_audio_url', true );
		?>
		<?php if( is_single() && ( $pod_subscribe_single == true || $pod_download == true ) ) : ?>
			<div id="audioinfo">
				<?php if( $pod_download == true ) : ?>
					<a class="butn small" href="<?php echo $pod_filedownload; ?>"><span class="batch space-r" data-icon="&#xF0BD;"></span> <?php echo __('Download Audio', 'thstlang'); ?></a>
				<?php endif; ?>
				<?php if( $pod_butn_one != '' && $pod_subscribe_single == true ) : ?>
					<a class="butn small" href="<?php echo $pod_butn_one_url; ?>"><?php echo $pod_butn_one; ?></a>
				<?php endif; ?>
			</div><!-- #audioinfo -->
		<?php endif; ?>		
		<?php if ( ! is_single() && ! is_sticky() ) : ?>
		<?php get_template_part('post/postheader'); ?>
		<div class="featured-media">
			<?php 
				if($audioembed != '') {
					$au_embedcode = wp_oembed_get( $audioembed );
					echo '<div class="audio_player au_oembed">' . $au_embedcode . '</div><!--audio_player-->';
				} elseif($audiourl != '') {
					echo '<div class="audio_player">' . do_shortcode('[audio src="' . $audiourl . '"][/audio]</div><!--audio_player-->');
				} elseif( is_array( $audioplists ) ) {
					echo do_shortcode('[playlist type="audio" ids="'.implode(',', array_keys($audioplists)).'"][/playlist]');
				} elseif ( $audioembedcode != '') {
					echo '<div class="audio_player embed_code">' . $audioembedcode . '</div><!--audio_player-->';
				} 
				
				if($audiocapt != '') {
					echo '<div class="audio-caption">' . $audiocapt . '</div>';
				}
				
			?>

		</div><!-- .featured-media -->
		<?php endif; ?>
		
		<?php if ( is_search() || is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'thstlang' ) ); ?>
		
			
			



		</div><!-- .entry-content -->
		<?php endif; ?>
		
		<?php get_template_part('post/postfooter'); ?>
	</article><!-- #post -->
	
	<style>
#my_popup {background: none repeat scroll 0 0 #fff;
    border-radius: 30px;
 padding: 3%;
    width: 50%;
    border: 5px solid;
    }
    .my_popup_close {
    background: none repeat scroll 0 0 #000;
    border: medium none;
    border-radius: 16px;
    color: #fff;
    float: right;
    font-weight: bold;
    margin: -54px -45px 0 0;
}
</style>



<!--  <div id="my_popup">-->
<!--  <button class="my_popup_close">X</button>-->
<!--  <p>Login required for download this podcast or video and if logged in then it will start download automatically</p>-->
<!-- -->
<!--</div>-->


<!--<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>-->
<!--<script src="http://vast-engineering.github.io/jquery-popup-overlay/jquery.popupoverlay.js"></script>-->
<!---->
<!--  <script>-->
<!--    $(document).ready(function() {-->
<!---->
<!--      // Initialize the plugin-->
<!--      $('#my_popup').popup();-->
<!---->
<!--    });-->
<!--  </script>-->