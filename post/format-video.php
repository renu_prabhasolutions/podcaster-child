<?php
/**
 * This file is used for your video post format.
 * @package Podcaster
 * @since 1.0
 * @author Theme Station : http://www.themestation.net
 * @copyright Copyright (c) 2014, Theme Station
 * @link http://www.themestation.net
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
 
 //$videoembed = apply_filters( 'thst_the_action', get_post_meta( $post->ID, 'cmb_thst_video_embed', true ) );
 //$videourl = apply_filters( 'thst_the_action', get_post_meta( $post->ID, 'cmb_thst_video_url', true ) );

 $videoembed = get_post_meta( $post->ID, 'cmb_thst_video_embed', true );
 $videourl = get_post_meta( $post->ID, 'cmb_thst_video_url', true );
 $videocapt = get_post_meta($post->ID, 'cmb_thst_video_capt', true);
 $videoplists = get_post_meta( $post->ID, 'cmb_thst_video_playlist', true );
 $videothumb = get_post_meta($post->ID, 'cmb_thst_video_thumb',true);
 $videoembedcode = get_post_meta( $post->ID, 'cmb_thst_video_embed_code', true );
 ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php 
			$pod_download = get_post_meta( $post->ID, 'cmb_thst_video_download', true );
			$pod_filedownload = get_post_meta( $post->ID, 'cmb_thst_video_url', true );
		?>
		<?php if( is_single() && $pod_download == true ) : ?>
			<div id="audioinfo">
				<?php if( $pod_download == true ) : ?>
					<a class="butn small" href="<?php echo $pod_filedownload; ?>"><span class="batch space-r" data-icon="&#xF0BD;"></span> Download Video</a>
				<?php endif; ?>
			</div><!-- #audioinfo -->
		<?php endif; ?>
		<?php get_template_part('post/postheader'); ?>
		
		<?php if ( ! is_single() && ! is_sticky()  ) : ?>
		<div class="featured-video featured-media">
			
				<?php if( $videoembed != '' ) {
				 	echo '<div class="video_player">' .  wp_oembed_get($videoembed) . '</div><!--video_player-->';
				} elseif( $videourl != '' ){
					echo '<div class="video_player">' . do_shortcode('[video poster="' .$videothumb. '" src="' .$videourl. '"][/video]') .'</div><!--video_player-->';
				} elseif( is_array( $videoplists ) ) {
					echo do_shortcode('[playlist type="video" ids="'.implode(',', array_keys($videoplists)).'"][/playlist]');
				} elseif( $videoembedcode !='') {
					echo '<div class="video_player">' . $videoembedcode . '</div><!--video_player-->';									
				} else {
					//Do Nothing.
				}
				if( $videocapt != '' ) {
					echo '<div class="video-caption">' . $videocapt . '</div>';
				}
				?>

		</div><!-- .featured-media -->
		<?php endif; ?>

		<?php if ( is_archive() || is_search() ) : // Only display Excerpts for Search ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->
		<?php elseif ( is_single() ) : ?>
			<div class="entry-content">
				<?php the_content(); ?>
			</div><!-- .entry-content -->
		<?php else : ?>
			<div class="entry-content">
				<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'thstlang' ) ); ?>
			</div><!-- .entry-content -->
		<?php endif; ?>
		<?php get_template_part('post/postfooter'); ?>
	</article><!-- #post -->