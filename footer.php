<?php
/**
 * This file is used for your footer.
 *
 * @package Podcaster
 * @since 1.0
 * @author Theme Station
 * @copyright Copyright (c) 2014, Theme Station
 * @link http://www.themestation.co
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
$options = get_option('podcaster-theme');  
if(isset($options['pod-footer-text'])){
	$pod_footer_text = $options['pod-footer-text'];
}


if( isset( $options['pod-facebook'] ) ){
	$pod_facebook = $options['pod-facebook'];
}
if( isset( $options['pod-twitter'] ) ){
	$pod_twitter = $options['pod-twitter'];
}
if( isset( $options['pod-google'] ) ){
	$pod_google = $options['pod-google'];
}
if( isset( $options['pod-instagram'] ) ){
	$pod_instagram = $options['pod-instagram'];
}
if( isset( $options['pod-tumblr'] ) ){
	$pod_tumblr = $options['pod-tumblr'];
}
if( isset( $options['pod-pinterest'] ) ){
	$pod_pinterest = $options['pod-pinterest'];
}
if( isset( $options['pod-flickr'] ) ){
	$pod_flickr = $options['pod-flickr'];
}
if( isset( $options['pod-youtube'] ) ){
	$pod_youtube = $options['pod-youtube'];
}
if( isset( $options['pod-vimeo'] ) ){
	$pod_vimeo = $options['pod-vimeo'];
}
if( isset( $options['pod-skype'] ) ){
	$pod_skype = $options['pod-skype'];
}
if( isset( $options['pod-dribbble'] ) ){
	$pod_dribbble = $options['pod-dribbble'];
}
if( isset( $options['pod-weibo'] ) ){
	$pod_weibo = $options['pod-weibo'];
}
if( isset( $options['pod-foursquare'] ) ){
	$pod_foursquare = $options['pod-foursquare'];
}
if( isset( $options['pod-github'] ) ){
	$pod_github = $options['pod-github'];
}
if( isset( $options['pod-xing'] ) ){
	$pod_xing = $options['pod-xing'];
}
?>

<?php /*?><footer class="main">
	<div class="footer-widgets">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8">
					<div class="footer-inner">
						<?php if( isset( $pod_footer_text ) && $pod_footer_text != '' ) : ?>
							<?php echo $pod_footer_text; ?>
						<?php endif; ?>						
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<div class="footer-inner social_container">
						<?php if( isset( $pod_facebook ) && $pod_facebook !="" ) { ?>
							<a class="facebook social_icon" href="<?php echo $pod_facebook ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_twitter ) && $pod_twitter !="" ) { ?>
							<a class="twitter social_icon" href="<?php echo $pod_twitter ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_google ) && $pod_google !="" ) { ?>
						<a class="google social_icon" href="<?php echo $pod_google ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_instagram ) && $pod_instagram !="" ) { ?>
						<a class="instagram social_icon" href="<?php echo $pod_instagram ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_tumblr ) && $pod_tumblr !="" ) { ?>
						<a class="tumblr social_icon" href="<?php echo $pod_tumblr ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_pinterest ) && $pod_pinterest !="" ) { ?>
						<a class="pinterest social_icon" href="<?php echo $pod_pinterest ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_flickr ) && $pod_flickr !="" ) { ?>
						<a class="flickr social_icon" href="<?php echo $pod_flickr ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_youtube ) && $pod_youtube !="" ) { ?>
						<a class="youtube social_icon" href="<?php echo $pod_youtube ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_vimeo ) && $pod_vimeo !="" ) { ?>
						<a class="vimeo social_icon" href="<?php echo $pod_vimeo ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_skype ) && $pod_skype !="" ) { ?>
						<a class="skype social_icon" href="<?php echo $pod_skype ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_dribbble ) && $pod_dribbble !="" ) { ?>
						<a class="dribbble social_icon" href="<?php echo $pod_dribbble ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_weibo ) && $pod_weibo !="" ) { ?>
						<a class="weibo social_icon" href="<?php echo $pod_weibo ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_foursquare ) && $pod_foursquare !="" ) { ?>
						<a class="foursquare social_icon" href="<?php echo $pod_foursquare ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_github ) && $pod_github !="" ) { ?>
						<a class="github social_icon" href="<?php echo $pod_github ?>"></a>
						<?php } ?>
						<?php if( isset( $pod_xing ) && $pod_xing !="" ) { ?>
						<a class="xing social_icon" href="<?php echo $pod_xing ?>"></a>
						<?php } ?>
					</div>
				</div><!-- .col -->
			</div>
		</div>
	</div>
</footer><?php */?>
</div>
<div class="postfooter">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4 col-md-4">
						<span><?php echo get_bloginfo( 'name' ); ?></span> &copy; <?php echo date("Y") ?>
					</div><!-- .col -->
					<div class="col-lg-8 col-md-8">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'depth' => 1,  'sort_column' => 'menu_order', 'menu_class' => 'thst-menu', 'fallback_cb' => false, 'container' => 'nav' )); ?>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .col -->
		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- .post-footer -->

</div><!--end .supercontainer-->
<?php if ( current_user_can('administrator') ) { ?>
<?php wp_footer(); /* Footer hook, do not delete, ever */ ?>
<style>
.login_area{ display:none;}
</style>
<?php } else { ?>
<style>
html {
    margin-top: 0 !important;
}
</style>
<?php } ?>

</body>
</html>